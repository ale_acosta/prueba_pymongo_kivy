from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from pymongo import MongoClient
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup
mongohost = 'mongodb://localhost'       # ubicación del servidor a través del protocolo de mongodb. En este caso se conecta a localhost, por defecto en el puerto 27017. LLegado el momento de utilizar un servidor real remplazar "localhost" por ip y puerto asignados.
client = MongoClient(mongohost)         # conecta a mongodb y genera la conexión a una base de datos especifica.
db = client['login']                    # variable en la cual se guarda una base de datos de los datos de usuarios ingresados, la cual es seleccionada desde client (o creada, de no existir)
basetouch = client['dbtouch']           # base de datos donde se guardan datos touch (o de cursor).
coltach= basetouch['coltouch']          # colección donde se alojan los documentos con los datos touch.
mmovex = 0                              # variables globales utilizadas para almacenar eventos touch para luego guardarlos en los doumentos json.
mmovey = 0
ddownx= 0
ddowny= 0
uupy= 0
uupx= 0

class Touch(Widget):                    # se instancia la clase Touch para registrar los eventos touch o de cursor en una pantalla popup utlizada como contexto o "pad".



    def on_touch_move(self, touch):
        global mmovex                   #se utiliza para llamar a la variable global. Si bien es reconocida por estar declarada al inicio, si no la llamamos inicializará en cero y solo guardará el ultimo evento.
        global mmovey
        mmovex = touch.x
        mmovey = touch.y
        print("mueve x", mmovex, "mueve y", mmovey)
        coltach.insert_one({"movex": mmovex, "it": mmovey})     # se guardan uno a uno, en un documento json, los eventos touch de movimiento almacenados en las variables. En la colección coltach, base de datos dbtouch

    def on_touch_down(self, touch):
        global ddownx
        global ddowny
        ddowny = touch.y
        ddownx = touch.x
        print("down y: ", ddowny, "down x: ", ddownx)
        coltach.insert_one({"down y: ": ddowny, "down x: ": ddownx}) # se guardan uno a uno, en un documento json, los eventos touch down almacenados en las variables.

    def on_touch_up(self, touch):
        global uupx
        global uupy
        uupy = touch.y
        uupx = touch.x
        print("up x: ", uupy, "up y: ", uupx)
        coltach.insert_one({"up y: ": uupy, "up x:": uupx})     # se guardan uno a uno, en un documento json, los eventos touch up almacenados en las variables.

class MainWindow(Screen):                                       # se instancia la clase mainwindows en la cual tendrá lugar el login de usuarios
    username = ObjectProperty(None)                             # a través de la función id de labels creados en .kv se llama a la s propiedades de tales objetos.
    mail = ObjectProperty(None)
    password = ObjectProperty(None)

    def btn(self):                                              # se convierte a texto los datos ingresados en los labels de .kv para ser almacenados en la colección "users"

        user = self.username.text
        usermail = self.mail.text
        userpswrd = self.password.text
        colle = db['users']
        colle.insert_one({'name': user, 'mail': usermail, 'password': userpswrd})
        print(self.username.text)
        self.username.text = ""
        self.mail.text = ""
        self.password.text = ""

class SecondWindow(Screen, FloatLayout):

    def btn1(self):                     #se define un objeto (botón) con un metodo que muestre la ventana popup donde se dara contexto a los eventos de la clase Touch
        show_popup()

    def btnfind(self):                  #se define un objeto (botón) con metodo para devolver los eventos touch almacenados en documentos json a través de la conexión a mongodb
        datadraw = client.dbtouch.coltouch.find({})
        for document in datadraw: print(document)

    def btndelete(self):                #se define un objeto (botón) para eliminar los eventos touch almacenados en documentos json en mongodb
        client.dbtouch.coltouch.drop()
        print("removed")

class Pop(FloatLayout):
    global show_popup
    def show_popup():                   # Se define propiedades y metodo de un objeto ventana popup para la clase Pop (es global para que se defina cuando es llamada por el objeto boton btn1 de la clase secondwindows)
        show = Pop()

        popupWindow = Popup(title="your_touch_Sound", content=show, size_hint=(None, None), size=(400, 400))

        popupWindow.open()

class WindowManager(ScreenManager):
    pass

kv = Builder.load_file("my.kv")     # se asigna un metodo a la clase  Builder para cargar el archivo kv cuando la variable ea retornada en la instancia mymainapp

class MyMainApp(App):
    def build(self):
        return kv

if __name__ == "__main__":
    MyMainApp().run()
